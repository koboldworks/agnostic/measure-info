# Change Log

## 1.0

- Foundry v12 compatibility
- Fixed measured template handling for Foundry v11.

## 0.5.0

- New release mechanism for smaller download & install sizes.

## 0.4.0

- Changed: Move icon replaced with move cursor to make it more clear you don't need to use the icon.
- Removed: libWrapper shim, the module is now explicitly required.
- Foundry v10 compatibility

## 0.3.0

- New: Measured templates are shown for current user also.
- Removed: Support for Foundry 0.7 and 0.8

## 0.2.2

- Fix: Potential fix for errors on load if someone was actively measuring.
- Changed: Small performance boost.

## 0.2.1

- Fix: Interface sizes weirdly sometimes.
- Changed: The interface turns somewhat transparent on mouse over during active measurement.  
  This can be overridden with `#measurement-info.active:hover` CSS selector.
- Removed: Secret GM option. Set this in Foundry permission settings instead for exactly the same effect.

## 0.2.0 Added ability to move the dialog

- New: Dialog dragging to move it. Location is saved.
- New: Simple API functions accessible via the module's .api namespace.  
  `game.modules.get('koboldworks-measure-info').api`
  Included functions: hud.reset(), hud.setPosition(), and hud.save()
- Removed: Clickthrough no longer works as it interfered with moving the dialog.

## 0.1.0 Initial release
