const CFG = {
	module: 'koboldworks-measure-info',
	SETTINGS: {
		fade: 'fadeTime',
		position: 'offset',
		templates: 'templates',
		mimicRuler: 'mimicRuler',
		keepUsers: 'keepUsers',
	},
	COLORS: {
		main: 'color:mediumseagreen;',
		number: 'color:mediumpurple',
		unset: 'color:unset',
	},
};

// V10 compatibility
const getDocData = (doc) => game.release?.generation >= 10 ? doc : doc?.data;

/**
 * @param {number} num
 * @param {number} decimalPlaces
 * @param {"round"|"ceil"|"floor"} type Rounding function
 */
const maxPrecision = (num, decimalPlaces = 0, type = 'round') => {
	const p = Math.pow(10, decimalPlaces || 0),
		n = num * p * (1 + Number.EPSILON);
	return Math[type](n) / p;
};

class RayData {
	ray = undefined;
	canvas = undefined;
	width = undefined;
	height = undefined;

	/**
	 *
	 * @param {Ray} ray
	 * @param {{size: number, distance: number}} canvas
	 */
	constructor(ray, { canvas, isSquare = false } = {}) {
		this.ray = ray;
		const { size, distance } = canvas.dimensions;
		this.canvas = { size, distance };
		if (isSquare) {
			const { width, height } = ray.bounds;
			this.width = width / size * distance;
			this.height = height / size * distance;
		}
	}

	get distance() {
		const { size, distance } = this.canvas;
		return this.ray.distance / size * distance; // Adjust to match ruler data;
	}
}

class RulerInfo {
	/** @type {RulerUI} */
	static #hud;

	constructor() {
		RulerInfo.#hud = new RulerUI();
	}

	get hud() {
		return RulerInfo.#hud;
	}

	measure(wrapped, ...args) {
		const rv = wrapped(...args);
		if (rv?.length > 0) {
			RulerInfo.#hud.update(rv, { user: this.user });
		}
		return rv;
	}

	clear(wrapped, ...args) {
		RulerInfo.#hud.end(this.user.id);
		return wrapped(...args);
	}

	static async #templateUpdate(ray, isSquare = false) {
		// Compatibility with Ruler data
		const rayData = new RayData(ray, { canvas, isSquare });
		RulerInfo.#hud.update([rayData], { user: game.user, isSquare });

		/*
		if (game.settings.get(CFG.module, CFG.SETTINGS.mimicRuler)) {
			const msg = { distance: rayData.distance, isSquare, active: true };
			if (isSquare) {
				msg.width = rayData.width;
				msg.height = rayData.heigh;
			}
			game.socket.emit(this.socketFlag, msg);
		}
		*/
	}

	/**
	 * Signal end of template drawing.
	 */
	static async #templateUpdateEnd() {
		// game.socket.emit(this.socketFlag, { active: false });
	}

	// Foundry v10 and prior
	async refreshTemplate(wrapped) {
		const rv = await wrapped();

		// Ignore refresh on already placed templates
		if (this.id !== null) return rv;

		const isSquare = rv.shape instanceof NormalizedRectangle;
		RulerInfo.#templateUpdate(rv.ray, isSquare);

		return rv;
	}

	// Foundry v12 shape update
	refreshTemplateShape(wrapped, ...args) {
		const rv = wrapped(...args);

		if (this.id !== null) return rv;

		const isSquare = this.shape.type === PIXI.SHAPES.RECT;
		RulerInfo.#templateUpdate(this.ray, isSquare);

		return rv;
	}

	// Foundry v11
	refreshTemplateShape11(wrapped, flags, ...args) {
		const rv = wrapped(flags, ...args);

		if (this.id !== null) return rv;

		if (flags.refreshShape) {
			const isSquare = this.shape.type === PIXI.SHAPES.RECT;
			RulerInfo.#templateUpdate(this.ray, isSquare);
		}

		return rv;
	}

	destroyTemplate(wrapped, ...args) {
		RulerInfo.#hud.end(game.user.id);
		RulerInfo.#templateUpdateEnd();
		return wrapped(...args);
	}
}

class Point {
	x = 0;
	y = 0;
	z = 0;
	constructor(x, y, z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	static fromRayPoint(rayPoint) {
		return new Point(rayPoint.x, rayPoint.y, 0);
	}

	toString() {
		return `XYZ(${this.x},${this.y},${this.z})`;
	}
}

class UserInfo {
	/** @type {string} */
	id;
	/** @type {string} */
	name;
	/** @type {boolean} */
	active = false;
	/** @type {HTMLElement} */
	element;
	/** @type {HTMLElement} */
	label;
	/** @type {null | number} */
	hideTimer = null;

	/**
	 * Starting point of measurement.
	 *
	 * @type {null|Point}
	 */
	startPoint = null;
	/** @type {null|Point} */
	endPoint = null;

	constructor(id, name, el, label) {
		this.id = id;
		this.name = name;
		this.element = el;
		this.label = label;
		// this.#data = game.users.get(id);
	}

	/** @type {UserData} */
	// #data;

	start() {
		this.element.classList.add('active');
		this.active = true;
	}

	stop() {
		this.element.classList.remove('active');
		this.active = false;
		this.startPoint = null;
		this.endPoint = null;
	}

	equals(obj) {
		return obj;
	}
}

class RulerUI {
	id = 'measurement-info';

	/** @type {HTMLElement} */
	el;
	children;

	active = 0;
	/** @type {Object.<string,UserInfo} */
	users = {};

	defaultX = 160;
	defaultY = 120;

	reset() {
		this.setPosition(this.defaultX, this.defaultY);
	}

	setPosition(x, y) {
		// TODO: Validate position to be within view
		this.el.style.left = `${x}px`;
		this.el.style.top = `${y}px`;
	}

	save() {
		const C = CFG.COLORS, x = this.el.offsetLeft, y = this.el.offsetTop;
		console.log(`%cMEASURE INFO%c | Save | Position | X:%c${x}%c Y:%c${y}%c`,
			C.main, C.unset, C.number, C.unset, C.number, C.unset);
		game.settings.set(CFG.module, CFG.SETTINGS.position, { x, y });
	}

	dOX = 0;
	dOY = 0;

	dragEvent(ev) {
		ev.preventDefault();
		ev.stopPropagation();
		this.setPosition(ev.clientX - this.dOX, ev.clientY - this.dOY);
	}

	stopDrag(ev) {
		ev.preventDefault();
		ev.stopPropagation();
		document.removeEventListener('mousemove', this.dragEventBound);
		document.removeEventListener('mouseup', this.stopDragBound);
		this.save();
	}

	stopDragBound = this.stopDrag.bind(this);
	dragEventBound = this.dragEvent.bind(this);

	startDrag(ev) {
		ev.preventDefault();
		ev.stopPropagation();
		document.addEventListener('mousemove', this.dragEventBound);
		document.addEventListener('mouseup', this.stopDragBound);
		this.dOX = ev.offsetX;
		this.dOY = ev.offsetY;
	}

	registerDrag() {
		this.el.addEventListener('mousedown', this.startDrag.bind(this));
	}

	render() {
		this.el = document.createElement('div');
		this.el.id = this.id;
		this.el.classList.add('flexcol');
		const label = document.createElement('label');
		label.textContent = game.i18n.localize('Koboldworks.MeasureInfo.Title');
		this.el.appendChild(label);
		this.el.appendChild(document.createElement('hr'));

		this.children = document.createElement('ul');
		this.children.classList.add('user-list');
		this.el.appendChild(this.children);
		document.body.appendChild(this.el);

		const savedPos = game.settings.get(CFG.module, CFG.SETTINGS.position);
		this.setPosition(savedPos.x, savedPos.y);

		for (const user of game.users)
			if (user.id !== game.user.id) $(this.fillUser(user.id, user.name).element).hide();

		this.registerDrag();

		const fadeTime = game.settings.get(CFG.module, CFG.SETTINGS.fade);
		if (fadeTime > 0) $(this.el).hide();
	}

	fillUser(userId, name) {
		if (userId in this.users) return;

		const li = document.createElement('li');
		li.classList.add('user');
		li.classList.add('flexrow');
		if (game.user.id === userId) li.classList.add('self');

		li.setAttribute('data-user-id', userId);
		const uName = document.createElement('label');
		uName.classList.add('name');
		uName.textContent = name;
		const measure = document.createElement('label');
		measure.textContent = 'n/a';
		measure.classList.add('distance');
		li.appendChild(uName);
		li.appendChild(measure);

		this.children.appendChild(li);

		const user = new UserInfo(userId, name, li, measure);
		this.users[userId] = user;
		return user;
	}

	showUser(userId) {
		const user = this.users[userId];
		if (!user) return;
		if (user.hideTimer !== null) clearTimeout(user.hideTimer);
		user.hiderId = null;
		$(user.element).stop().fadeIn(200);
	}

	hideUser(userId) {
		const user = this.users[userId];
		if (!user) return;

		const fadeTime = game.settings.get(CFG.module, CFG.SETTINGS.fade);
		if (fadeTime === 0) return;

		// TODO: Apply minor fading but maintain visibility to signal old measurements
		if (user.hideTimer !== null) clearTimeout(user.hideTimer);

		user.hideTimer = setTimeout(() => {
			if (!game.settings.get(CFG.module, CFG.SETTINGS.keepUsers))
				$(user.element).stop().fadeOut(500);
			this.tryHide();
		}, fadeTime * 1000);
	}

	displayId = null;
	show() {
		if (this.hiderId !== null) clearTimeout(this.hiderId);
		this.hiderId = null;
		$(this.el).fadeIn(200);
	}

	hiderId = null;
	tryHide() {
		if (this.activeMeasures > 0) return;

		const fadeTime = game.settings.get(CFG.module, CFG.SETTINGS.fade);
		if (fadeTime === 0) return;

		$(this.el).stop().fadeOut(500);
	}

	activeMeasures = 0;

	/**
	 *
	 * @param {RayData[]} rays
	 * @param {object} options
	 * @param {boolean} options.isSquare
	 * @param {UserData} options.user
	 */
	update(rays, { user, isSquare = false } = {}) {
		user = this.users[user.id] ?? this.fillUser(user.id, user.name);
		const total = rays.reduce((a, b) => a + b.distance, 0);

		// toFixed(#)/1 is a dumb trick to have maximum precision but cut down zeroes

		const fRay = rays[0],
			lRay = rays[rays.length - 1];

		const v10 = game.release.generation >= 10;
		const scene = canvas.scene;
		const gUnits = v10 ? scene.grid.units : scene.data.gridUnits;

		if (!isSquare)
			user.label.textContent = `${maxPrecision(total, 3)} ${gUnits}`;
		else {
			const { width, height } = fRay;
			user.label.textContent = `${Math.roundDecimals(width, 1)} × ${Math.roundDecimals(height, 1)} [${Math.roundDecimals(total, 1)}] ${gUnits}`;
		}

		if (!user.active) {
			user.start();
			this.activeMeasures++;
			user.element.classList.add('active');
		}

		user.startPoint ??= Point.fromRayPoint(fRay.ray.A);

		const newEndPoint = Point.fromRayPoint(lRay.ray.B);
		if (newEndPoint != user.endPoint) {
			user.endPoint = newEndPoint;
			// console.log("Start:", user.startPoint.toString(), "End:", user.endPoint.toString());
		}

		this.el.classList.add('active');

		this.show();
		this.showUser(user.id);
	}

	async end(userId) {
		if (!(userId in this.users)) return;
		const user = this.users[userId];

		if (user.active) {
			user.stop();
			this.activeMeasures--;
			if (this.activeMeasures <= 0) this.el.classList.remove('active');
			user.element.classList.remove('active');
		}

		this.hideUser(userId);
	}

	clear() {
		this.el?.remove();
	}
}

const mi = new RulerInfo();

Hooks.once('init', () => {
	// Dialog position
	game.settings.register(CFG.module, CFG.SETTINGS.position, {
		type: Object,
		default: {},
		scope: 'client',
		config: false,
	});

	// Fade time
	game.settings.register(CFG.module, CFG.SETTINGS.fade, {
		name: 'Koboldworks.MeasureInfo.FadeLabel',
		hint: 'Koboldworks.MeasureInfo.FadeHint',
		type: Number,
		default: 5,
		range: { min: 0, max: 300, step: 1 },
		scope: 'client',
		config: true,
	});

	// Keep users
	game.settings.register(CFG.module, CFG.SETTINGS.keepUsers, {
		name: 'Koboldworks.MeasureInfo.KeepUsersLabel',
		hint: 'Koboldworks.MeasureInfo.KeepUsersHint',
		type: Boolean,
		default: false,
		scope: 'client',
		config: true,
	});

	// Template support
	game.settings.register(CFG.module, CFG.SETTINGS.templates, {
		name: 'Koboldworks.MeasureInfo.TemplateLabel',
		hint: 'Koboldworks.MeasureInfo.TemplateHint',
		type: Boolean,
		default: true,
		scope: 'client',
		config: true,
	});

	/*
	game.settings.register(CFG.module, CFG.SETTINGS.mimicRuler, {
		name: 'Koboldworks.MeasureInfo.MimicRulerLabel',
		hint: 'Koboldworks.MeasureInfo.MimicRulerHint',
		type: Boolean,
		default: true,
		scope: 'world',
		config: true,
	});
	*/

	// API
	game.modules.get(CFG.module).api = {
		hud: {
			reset: () => mi.hud.reset(),
			setPosition: (x, y) => mi.hud.setPosition(x, y),
			savePosition: () => mi.hud.save(),
		},
	};
});

Hooks.once('ready', () => {
	mi.hud.render();

	const mod = game.modules.get(CFG.module);

	/* global libWrapper */

	libWrapper.register(CFG.module, 'Ruler.prototype.clear', mi.clear, 'WRAPPER');
	libWrapper.register(CFG.module, 'Ruler.prototype.measure', mi.measure, 'WRAPPER');

	const v11 = game.release.generation >= 11;
	const v12 = game.release.generation >= 12;

	if (game.settings.get(CFG.module, CFG.SETTINGS.templates)) {
		// V11
		if (v11) {
			libWrapper.register(CFG.module, 'CONFIG.MeasuredTemplate.objectClass.prototype._applyRenderFlags', mi.refreshTemplateShape11, 'WRAPPER');
			libWrapper.register(CFG.module, 'CONFIG.MeasuredTemplate.objectClass.prototype._destroy', mi.destroyTemplate, 'WRAPPER');
		}
		// V12
		else if (v12) {
			libWrapper.register(CFG.module, 'CONFIG.MeasuredTemplate.objectClass.prototype._refreshShape', mi.refreshTemplateShape, 'WRAPPER');
			libWrapper.register(CFG.module, 'CONFIG.MeasuredTemplate.objectClass.prototype._destroy', mi.destroyTemplate, 'WRAPPER');
		}
		// V10 and earlier
		else {
			libWrapper.register(CFG.module, 'CONFIG.MeasuredTemplate.objectClass.prototype.refresh', mi.refreshTemplate, 'WRAPPER');
			libWrapper.register(CFG.module, 'CONFIG.MeasuredTemplate.objectClass.prototype.destroy', mi.destroyTemplate, 'WRAPPER');
		}
	}

	/*
	libWrapper.register(CFG.module, 'SquareGrid.prototype.measureDistances', mi.measureDistances, 'WRAPPER');
	libWrapper.register(CFG.module, 'HexagonalGrid.prototype.measureDistances', mi.measureDistances, 'WRAPPER');
	libWrapper.register(CFG.module, 'BaseGrid.prototype.measureDistances', mi.measureDistances, 'WRAPPER');
	*/

	const md = getDocData(mod);
	const C = CFG.COLORS;
	console.log(`%cMEASURE INFO%c | %c${md.version}%c | READY`, C.main, C.unset, C.number, C.unset);

	// Hacky old setting removal
	if (game.settings.settings.get(CFG.module, 'secretGM') !== undefined)
		game.settings.settings.delete(`${CFG.module}.secretGM`);
});
