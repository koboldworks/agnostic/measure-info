# Koboldworks – Measurement Info

Displays measurement distances.

This also displays info on measurement templates too as they're drawn for current user.

The dialog fades away by default few seconds after no longer being used.

![Display](./img/screencaps/display.png)

Active measuring is highlighted, and your own measurement is emphasized.

## Configuration

Currently available configuration options are as follows:

- Fade time. 0 disables it and makes the dialog permanent, displaying last measurement.
- Secret GM option, to hide measurement for GM players for non-GMs.

## Styling customization

`#measurement-info` is the starting point.

## API

`const api = game.modules.get('koboldworks-measure-info').api`

Reset HUD:  
`api.hud.reset()`

Set HUD position:  
`api.hud.setPosition(x, y)`

## Rationale

I can't read the ruler labels, okay?

## Compatibility

### Incompatible

- **Drag Ruler** [breaks Ruler.measure()](https://github.com/manuelVo/foundryvtt-drag-ruler/issues/219) that this module debends on.

## Install

Manifest URL: <https://gitlab.com/koboldworks/agnostic/measure-info/-/releases/permalink/latest/downloads/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
